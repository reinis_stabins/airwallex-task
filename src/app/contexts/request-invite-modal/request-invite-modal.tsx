import React from 'react';

interface RequestInviteModalContextType {
  readonly isOpen: boolean;
  readonly open: () => void;
  readonly close: () => void;
}

const RequestInviteModalContext = React.createContext<RequestInviteModalContextType | undefined>(
  undefined
);

interface Props {
  readonly children: React.ReactNode;
}

export const RequestInviteModalProvider = React.memo(({ children }: Props) => {
  const [isOpen, setIsOpen] = React.useState(false);

  const open = React.useCallback(() => {
    setIsOpen(true);
  }, []);

  const close = React.useCallback(() => {
    setIsOpen(false);
  }, []);

  const context = React.useMemo(
    () => ({
      isOpen,
      open,
      close,
    }),
    [isOpen, open, close]
  );

  return (
    <RequestInviteModalContext.Provider value={context}>
      {children}
    </RequestInviteModalContext.Provider>
  );
});

export const useRequestInviteModal = (): RequestInviteModalContextType => {
  const context = React.useContext(RequestInviteModalContext);

  if (!context)
    throw new Error('Please call useRequestInviteModal within RequestInviteModalProvider');

  return context;
};
