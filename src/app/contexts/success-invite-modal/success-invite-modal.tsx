import React from 'react';

interface SuccessInviteModalContextType {
  readonly isOpen: boolean;
  readonly open: () => void;
  readonly close: () => void;
}

const SuccessInviteModalContext = React.createContext<SuccessInviteModalContextType | undefined>(
  undefined
);

interface Props {
  readonly children: React.ReactNode;
}

export const SuccessInviteModalProvider = React.memo(({ children }: Props) => {
  const [isOpen, setIsOpen] = React.useState(false);

  const open = React.useCallback(() => {
    setIsOpen(true);
  }, []);

  const close = React.useCallback(() => {
    setIsOpen(false);
  }, []);

  const context = React.useMemo(
    () => ({
      isOpen,
      open,
      close,
    }),
    [isOpen, open, close]
  );

  return (
    <SuccessInviteModalContext.Provider value={context}>
      {children}
    </SuccessInviteModalContext.Provider>
  );
});

export const useSuccessInviteModal = (): SuccessInviteModalContextType => {
  const context = React.useContext(SuccessInviteModalContext);

  if (!context)
    throw new Error('Please call useSuccessInviteModal within SuccessInviteModalProvider');

  return context;
};
