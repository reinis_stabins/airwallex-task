import React, { useEffect } from 'react';
import { Modal } from 'antd';

import { useSuccessInviteModal } from '../../contexts/success-invite-modal';

export const SuccessInviteModal = React.memo(() => {
  const { isOpen, close } = useSuccessInviteModal();

  useEffect(() => {
    if (isOpen) {
      Modal.success({
        title: 'Invite request has been sent!',
        centered: true,
        afterClose: close,
      });
    }
  }, [isOpen, close]);

  return null;
});
