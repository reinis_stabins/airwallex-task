import React from 'react';
import { Button } from 'antd';

import { useRequestInviteModal } from '../../contexts/request-invite-modal';

export const RequestInviteButton = React.memo(() => {
  const { open } = useRequestInviteModal();

  return (
    <Button size="large" onClick={open}>
      Request an invite
    </Button>
  );
});
