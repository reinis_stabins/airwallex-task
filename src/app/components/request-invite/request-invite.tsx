import React from 'react';
import { Typography, Space } from 'antd';

import { RequestInviteModalProvider } from '../../contexts/request-invite-modal';
import { SuccessInviteModalProvider } from '../../contexts/success-invite-modal';
import { RequestInviteButton } from '../request-invite-button';
import { RequestInviteModal } from '../request-invite-modal';
import { SuccessInviteModal } from '../success-invite-modal';

export const RequestInvite = React.memo(() => (
  <SuccessInviteModalProvider>
    <RequestInviteModalProvider>
      <Typography.Title level={1}>A better way to enjoy a day.</Typography.Title>
      <Space size="large" direction="vertical">
        <Typography.Text>A better way to enjoy a day.</Typography.Text>
        <RequestInviteButton />
      </Space>
      <RequestInviteModal />
      <SuccessInviteModal />
    </RequestInviteModalProvider>
  </SuccessInviteModalProvider>
));
