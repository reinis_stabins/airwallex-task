import type { RequestInviteBody } from './types';

const REQUEST_INVITE_URL =
  'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth';

interface RequestInviteArgs {
  readonly body: RequestInviteBody;
}

export const requestInvite = async ({ body }: RequestInviteArgs): Promise<string> => {
  const response = await fetch(REQUEST_INVITE_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  });

  const result = await response.json();

  if (!response.ok) {
    throw new Error(result.errorMessage);
  }

  return result;
};
