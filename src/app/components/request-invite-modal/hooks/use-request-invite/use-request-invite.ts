import React, { useEffect, useMemo } from 'react';

import * as utils from './utils';
import type { RequestInviteBody } from './types';

const defaultResponse = {
  isLoading: false,
  data: undefined,
  error: undefined,
};

interface RequestInviteResponse {
  readonly isLoading: boolean;
  readonly data: boolean | undefined;
  readonly error: string | undefined;
}

interface RequestInvite {
  (body: RequestInviteBody): Promise<Record<'succeeded', boolean>>;
}

export const useRequestInvite = (): [RequestInviteResponse, RequestInvite] => {
  const [response, setResponse] = React.useState<RequestInviteResponse>(defaultResponse);
  const isMounted = React.useRef(true);

  const requestInvite = React.useCallback(async (body: RequestInviteBody) => {
    setResponse({
      isLoading: true,
      data: undefined,
      error: undefined,
    });

    try {
      const data = await utils.requestInvite({ body });
      if (isMounted.current) {
        setResponse({
          ...defaultResponse,
          isLoading: false,
          data: !!data,
        });
      }
      return { succeeded: true };
    } catch (error) {
      if (isMounted.current) {
        setResponse({
          isLoading: false,
          data: undefined,
          error: error.toString(),
        });
      }
      return { succeeded: false };
    }
  }, []);

  useEffect(
    () => () => {
      isMounted.current = false;
    },
    []
  );

  return useMemo(() => [response, requestInvite], [response, requestInvite]);
};
