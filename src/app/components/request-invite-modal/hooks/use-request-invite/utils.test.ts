import * as utils from './utils';

describe('requestInvite', () => {
  beforeAll(() => jest.spyOn(window, 'fetch'));

  it('should make a successful fetch call', async () => {
    (window.fetch as jest.Mock).mockResolvedValueOnce({
      ok: true,
      json: async () => 'success',
    });

    const request = await utils.requestInvite({
      body: {
        email: 'test',
        name: 'admin',
      },
    });

    expect(request).toBe('success');
  });

  it('should make a failed fetch call', async () => {
    (window.fetch as jest.Mock).mockResolvedValueOnce({
      ok: false,
      json: async () => ({
        errorMessage: 'http failure',
      }),
    });

    await expect(
      utils.requestInvite({
        body: {
          email: 'test',
          name: 'admin',
        },
      })
    ).rejects.toThrow('http failure');
  });
});
