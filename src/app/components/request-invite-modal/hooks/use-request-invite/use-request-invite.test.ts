import { renderHook, act } from '@testing-library/react-hooks';

import { useRequestInvite } from './use-request-invite';

describe('useRequestInvite', () => {
  beforeAll(() => jest.spyOn(window, 'fetch'));

  it('should return initial state of invite request', () => {
    const { result } = renderHook(useRequestInvite);

    expect(result.current[0]).toEqual({
      isLoading: false,
      data: undefined,
      error: undefined,
    });
  });

  it('should return fulfilled state of invite request', async () => {
    const { result } = renderHook(useRequestInvite);
    const [, requestInvite] = result.current;

    (window.fetch as jest.Mock).mockResolvedValueOnce({
      ok: true,
      json: async () => 'success',
    });

    await act(async () => {
      await requestInvite({
        email: 'test',
        name: 'admin',
      });
    });

    expect(result.current[0]).toEqual({
      isLoading: false,
      data: true,
      error: undefined,
    });
  });

  it('should return failed state of invite request', async () => {
    const { result } = renderHook(useRequestInvite);
    const [, requestInvite] = result.current;

    (window.fetch as jest.Mock).mockResolvedValueOnce({
      ok: false,
      json: async () => ({ errorMessage: 'failure' }),
    });

    await act(async () => {
      await requestInvite({
        email: 'test',
        name: 'admin',
      });
    });

    expect(result.current[0]).toEqual({
      isLoading: false,
      data: undefined,
      error: 'Error: failure',
    });
  });
});
