export interface RequestInviteBody {
  readonly name: string;
  readonly email: string;
}

// export interface RequestInviteResponse {

// }
