import React from 'react';
import { Space } from 'antd';

import { useRequestInviteModal } from '../../contexts/request-invite-modal';
import { Form } from './components/form';
import * as Styled from './styled';

export const RequestInviteModal = React.memo(() => {
  const { isOpen, close } = useRequestInviteModal();

  return (
    <Styled.Modal
      destroyOnClose
      onCancel={close}
      centered
      visible={isOpen}
      okButtonProps={{ disabled: true }}
      cancelButtonProps={{ disabled: true }}
    >
      <Styled.Container direction="vertical" size="large" as={Space}>
        <Styled.Title level={3}>Request an invite</Styled.Title>
        <Form closeModal={close} />
      </Styled.Container>
    </Styled.Modal>
  );
});
