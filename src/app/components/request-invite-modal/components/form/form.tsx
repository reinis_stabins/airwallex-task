import React from 'react';
import { useForm } from 'react-hook-form';
import { Input, Form as AntForm, Space } from 'antd';

import { resolver, fieldNames } from './schema';
import type { FormFields, FormData } from './schema';
import { useRequestInvite } from '../../hooks/use-request-invite';
import { useSuccessInviteModal } from '../../../../contexts/success-invite-modal';
import * as Styled from './styled';

interface Props {
  readonly closeModal: () => void;
}

export const Form = React.memo(({ closeModal }: Props) => {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<FormFields>({ resolver });

  const getError = React.useCallback(
    (field: string) => (field in errors ? errors[field]?.message : undefined),
    [errors]
  );

  const nameError = getError(fieldNames.name);
  const emailError = getError(fieldNames.email);
  const confirmEmailError = getError(fieldNames.confirmEmail);

  const [{ isLoading, error: requestInviteError }, requestInvite] = useRequestInvite();

  const { open: openSuccessInviteModal } = useSuccessInviteModal();

  const submit = React.useCallback(
    async ({ name, email }: FormData) => {
      const { succeeded } = await requestInvite({ name, email });

      if (succeeded) {
        closeModal();
        openSuccessInviteModal();
      }
    },
    [requestInvite, closeModal, openSuccessInviteModal]
  );

  const onSubmit = React.useMemo(() => handleSubmit(submit), [handleSubmit, submit]);

  return (
    <Styled.Form onSubmit={onSubmit} as={Space} direction="vertical" size="small">
      <AntForm.Item help={nameError} validateStatus={nameError ? 'error' : 'success'}>
        <Input
          aria-label={fieldNames.name}
          size="large"
          placeholder="Full name"
          {...register(fieldNames.name)}
        />
      </AntForm.Item>
      <AntForm.Item
        name={fieldNames.email}
        help={emailError}
        validateStatus={emailError ? 'error' : 'success'}
      >
        <Input
          aria-label={fieldNames.email}
          size="large"
          placeholder="Email"
          {...register(fieldNames.email)}
        />
      </AntForm.Item>
      <AntForm.Item
        name={fieldNames.confirmEmail}
        help={confirmEmailError}
        validateStatus={confirmEmailError ? 'error' : 'success'}
      >
        <Input
          aria-label={fieldNames.confirmEmail}
          size="large"
          placeholder="Confirm email"
          {...register(fieldNames.confirmEmail)}
        />
      </AntForm.Item>
      <Styled.Button onClick={onSubmit} type="primary" size="large" loading={isLoading}>
        Send
      </Styled.Button>
      <Styled.RequestFailureMessage
        style={{
          opacity: requestInviteError ? 1 : 0,
        }}
      >
        {requestInviteError}
      </Styled.RequestFailureMessage>
    </Styled.Form>
  );
});
