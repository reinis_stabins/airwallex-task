import { screen, waitFor } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { Form as AntForm } from 'antd';

import { renderWithTheme } from '../../../../../utils/render-with-theme';
import { Form } from './form';
import { fieldNames } from './schema';
import { SuccessInviteModalProvider } from '../../../../contexts/success-invite-modal';

describe('<Form />', () => {
  beforeAll(() => jest.spyOn(window, 'fetch'));

  it('should send a successful invite request', () => {
    (window.fetch as jest.Mock).mockResolvedValueOnce({
      ok: true,
      json: async () => 'success',
    });

    const closeModal = jest.fn();

    renderWithTheme(
      <AntForm>
        <SuccessInviteModalProvider>
          <Form closeModal={closeModal} />
        </SuccessInviteModalProvider>
      </AntForm>
    );

    const nameInput = screen.getByRole('textbox', { name: fieldNames.name });
    const emailInput = screen.getByRole('textbox', { name: fieldNames.email });
    const confirmEmailInput = screen.getByRole('textbox', { name: fieldNames.confirmEmail });
    const requestInviteButton = screen.getByRole('button');

    userEvent.type(nameInput, 'admin');
    userEvent.type(emailInput, 'test@gmail.com');
    userEvent.type(confirmEmailInput, 'test@gmail.com');

    userEvent.click(requestInviteButton);

    waitFor(() => {
      expect(closeModal).toBeCalled();
    });
  });
});
