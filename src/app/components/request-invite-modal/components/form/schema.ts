import * as yup from 'yup';
import type { Asserts } from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

export const fieldNames = {
  name: 'name',
  email: 'email',
  confirmEmail: 'confirmEmail',
};

export interface FormData {
  readonly name: string;
  readonly email: string;
  readonly confirmEmail: string;
}

const schema = yup.object().shape({
  [fieldNames.name]: yup
    .string()
    .min(3, 'Full name has to be at least 3 characters long')
    .required('Full name is required'),
  [fieldNames.email]: yup.string().email().required('Email is required'),
  [fieldNames.confirmEmail]: yup
    .string()
    .oneOf([yup.ref(fieldNames.email), null], 'Email and Confirm email should match')
    .required('Confirm email is required'),
});

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface FormFields extends Asserts<typeof schema> {}

export const resolver = yupResolver(schema);
