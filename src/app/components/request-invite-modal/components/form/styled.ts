import styled, { css } from 'styled-components';
import { Button as AntButton, Typography } from 'antd';
import { rem } from 'polished';

export const Form = styled.form`
  width: 100%;
`;

export const Button = styled(AntButton)`
  width: 100%;
`;

export const RequestFailureMessage = styled(Typography.Text).attrs({
  type: 'danger',
})`
  justify-content: center;
  display: flex;
  min-height: ${rem(22)};

  ${({ theme }) => css`
    transition: ease opacity ${theme.transition[300]}ms;
  `}
`;
