import styled from 'styled-components';
import { rem } from 'polished';
import { Modal as AntModal, Typography } from 'antd';

export const Modal = styled(AntModal)`
  .ant-modal-footer {
    display: none;
  }
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: center;
  padding-top: ${rem(20)};
`;

export const Title = styled(Typography.Title)`
  text-align: center;
`;
