import React from 'react';
import { Layout } from 'antd';

import { Header } from '../components/header';
import { Footer } from '../components/footer';
import { Content } from '../components/content';
import { RequestInvite } from './components/request-invite';

export const App = React.memo(() => (
  <Layout>
    <Header />
    <Content>
      <RequestInvite />
    </Content>
    <Footer />
  </Layout>
));
