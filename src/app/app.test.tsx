import { screen } from '@testing-library/react';
import { renderWithTheme } from '../utils/render-with-theme';

import { App } from './app';

describe('<App />', () => {
  it('should render', () => {
    renderWithTheme(<App />);
    const linkElement = screen.getByText(/Broccoli & Co/i);
    expect(linkElement).toBeInTheDocument();
  });
});
