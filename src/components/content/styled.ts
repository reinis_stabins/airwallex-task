import styled, { css } from 'styled-components';
import { Layout } from 'antd';
import { rem } from 'polished';

export const Content = styled(Layout.Content)`
  padding: 0 ${rem(50)};
  min-height: auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  ${({ theme }) => css`
    background-color: ${theme.colors.background.primary};
    margin-top: ${theme.layout.headerHeight};
  `};
`;
