import React from 'react';

import * as Styled from './styled';

interface Props {
  readonly children: React.ReactNode;
}

export const Content = React.memo(({ children }: Props) => (
  <Styled.Content>{children}</Styled.Content>
));
