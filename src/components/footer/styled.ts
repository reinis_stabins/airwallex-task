import styled, { css } from 'styled-components';
import { Layout } from 'antd';

export const Footer = styled(Layout.Footer)`
  display: flex;
  justify-content: center;
  align-items: center;

  ${({ theme }) => css`
    background-color: ${theme.colors.background.primary};
    border-top: 1px solid ${theme.colors.text.primary};
  `}
`;
