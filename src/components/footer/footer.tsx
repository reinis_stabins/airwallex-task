import React from 'react';
import { Typography } from 'antd';

import * as Styled from './styled';

export const Footer = React.memo(() => (
  <Styled.Footer>
    <Typography.Text>Footer</Typography.Text>
  </Styled.Footer>
));
