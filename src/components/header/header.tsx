import React from 'react';

import * as Styled from './styled';

export const Header = React.memo(() => (
  <Styled.Header>
    <Styled.Title level={4}>Broccoli & Co</Styled.Title>
  </Styled.Header>
));
