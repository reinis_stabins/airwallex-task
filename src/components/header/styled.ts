import styled, { css } from 'styled-components';
import { Layout, Typography } from 'antd';
import { rem } from 'polished';

export const Header = styled(Layout.Header)`
  display: flex;
  align-items: center;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  height: ${rem(80)};

  ${({ theme }) => css`
    background-color: ${theme.colors.background.primary};
    border-bottom: 1px solid ${theme.colors.text.primary};
  `};
`;

export const Title = styled(Typography.Title)`
  && {
    text-transform: uppercase;
    margin: 0;

    ${({ theme }) => css`
      color: ${theme.colors.text.primary};
    `};
  }
`;
