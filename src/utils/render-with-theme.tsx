import { ReactNode } from 'react';
import { render, RenderResult } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';

import { theme } from '../styles';

type RenderWithTheme = (component: ReactNode) => RenderResult;

export const renderWithTheme: RenderWithTheme = (component) => {
  const container = render(<ThemeProvider theme={theme}>{component}</ThemeProvider>);

  return {
    ...container,
    rerender: (rerenderedComponent: ReactNode) =>
      container.rerender(<ThemeProvider theme={theme}>{rerenderedComponent}</ThemeProvider>),
  };
};
