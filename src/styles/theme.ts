import { rem } from 'polished';

export const theme = {
  colors: {
    text: {
      primary: '#000000d9',
      secondary: '#ffffff',
    },
    background: {
      primary: '#ffffff',
    },
  },
  layout: {
    headerHeight: rem(80),
  },
  transition: {
    '300': 300,
    '500': 500,
  },
};
