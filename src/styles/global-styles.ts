import { createGlobalStyle } from 'styled-components/macro';
import { normalize } from 'polished';

export const GlobalStyles = createGlobalStyle`
  ${normalize()};

  html {
    height: 100%;
    width: 100%;
    margin: 0;
    padding: 0;
  }  
  
  body {
    overflow-x: auto;
    height: 100%;
    width: 100%;
  } 
  
  #root {
    height: 100%;
    width: 100%;
    position: relative;
    display: flex;
    flex-direction: column;
  }
`;
