import React from 'react';
import { ThemeProvider } from 'styled-components';

import { theme } from './theme';
import { GlobalStyles } from './global-styles';

interface Props {
  readonly children: React.ReactNode;
}

export const StylesProvider = React.memo(({ children }: Props) => (
  <ThemeProvider theme={theme}>
    <GlobalStyles />
    {children}
  </ThemeProvider>
));
