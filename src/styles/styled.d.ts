import 'styled-components/cssprop';
import { theme } from './theme';

type Theme = typeof theme;

declare module 'styled-components' {
  export interface DefaultTheme extends DefaultTheme, Theme {}
}
